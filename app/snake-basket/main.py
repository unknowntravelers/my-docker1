from heapq import nsmallest
from itertools import product

numbers = [0]*16
min_v = 0
for a in product(range(1, 7), repeat=4):
    numbers[sum(a)-min(a)-3] += 1

e = 0.0

for a in zip(range(3, 19), numbers):
    print(a)
    e += a[0]*a[1]

print("sum: " + str(sum(numbers)))
print("e: " + str(e/sum(numbers)))
